extern crate rand;
extern crate rustc_serialize as serialize;

pub mod authorizer {
    use pieces::rand::{Rng, thread_rng};
    use aes::aes_ecb;
    use pieces::serialize::base64::{STANDARD, FromBase64, ToBase64};

    fn parse(input: &str) -> Vec<(String, String)> {
        input.split("&").map(|kv| {
            let kv = kv.split("=").collect::<Vec<_>>();
            if kv.len() == 1 {
                // No value supplied
                (String::from(kv[0]), String::new())
            } else {
                (String::from(kv[0]), String::from(kv[1]))
            }
        }).collect()
    }

    fn profile_for(email: &str) -> String {
        let email = email.chars().filter(|c| *c != '=' && *c != '&').collect::<String>();
        let id = thread_rng().gen_range(0, 990);
        let role = "user";
        String::from([["email", &email].join("=").as_str(),
                      ["id", &id.to_string()].join("=").as_str(),
                      ["role", role].join("=").as_str()].join("&"))
    }

    pub struct Authorizer {
        key: Vec<u8>,
    }

    impl Authorizer {
        pub fn new(key: &[u8]) -> Authorizer {
            Authorizer{key: key.to_vec()}
        }
        pub fn provide_cookie(&self, email: &str) -> String {
            aes_ecb(profile_for(email).as_bytes(), &self.key, true, true).to_base64(STANDARD)
        }
        pub fn parse_cookie(&self, cookie: &str) -> Vec<(String, String)> {
            parse(&String::from_utf8(aes_ecb(&cookie.from_base64().unwrap(), &self.key, false, true)).unwrap())
        }
    }

    #[cfg(test)]
    mod tests {
        use pieces::authorizer::*;

        #[test]
        fn can_parse() {
            let input = "key=value&iam=vasya&id=10";
            let result = parse(input);
            assert_eq!(result.len(), 3);
            assert_eq!(result[0].0, String::from("key"));
            assert_eq!(result[2].1, String::from("10"));
        }

        #[test]
        fn generates() {
            let input = "some_test_email@example.com";
            let result = profile_for(input);
            assert_ne!(result.find(input), None);
            assert_ne!(result.find("role"), None);
            assert_ne!(result.find("id"), None);
        }

        #[test]
        fn wont_allow_metachars() {
            let input = "some_=test_ema&il@ex&&amp=&==&le.com";
            let result = profile_for(input);
            assert_ne!(result.find("some_test_email@example.com"), None);
        }

        #[test]
        fn basically_works() {
            let auth = Authorizer::new("YELLOW SUBMARINE".as_bytes());
            let email = "test@example.com";
            let cookie = auth.provide_cookie(email);
            let result = auth.parse_cookie(&cookie);
            println!("{:?}", result);
            assert_eq!(result.len(), 3);
            assert_ne!(result.iter().find(|(k, v)| k == "email" && v == email), None);
            assert_ne!(result.iter().find(|(k, v)| k == "role" && v == "user"), None);
            assert_ne!(result.iter().find(|(k, _)| k == "id"), None);
        }
    }
}
