// Quite some unused functions here since I'm using the same file for all the tasks of a set.
// Needs to be cleaned up later perhaps.
#![allow(dead_code)]

extern crate rustc_serialize as serialize;
use std::io::{BufReader, BufRead};
use std::fs::File;
use serialize::base64::{FromBase64};

pub mod xor;
pub mod aes;
pub mod pieces;

fn main() {
    let file = File::open("10.txt").unwrap();
    let reader = BufReader::new(file);
    let cipher = reader.lines().map(|l| l.unwrap()).collect::<Vec<String>>().join("").from_base64().unwrap();
    let key = "YELLOW SUBMARINE".as_bytes();
    let iv = vec![0u8; 16];
    let bitresult = aes::aes_cbc(&cipher, key, &iv, false);
    let result = String::from_utf8_lossy(&bitresult);
    println!("{}", result);
}
