extern crate openssl;
use aes::openssl::symm::{Cipher, Mode, Crypter};
extern crate rustc_serialize as serialize;
use aes::serialize::hex::FromHex;
use serialize::base64::{STANDARD, FromBase64, ToBase64};
extern crate rand;
use aes::rand::Rng;
use xor::lines_from_file;
use xor::xor;
use pieces::authorizer::Authorizer;

pub fn aes_ecb(input: &[u8], key: &[u8], encrypt: bool, finalize: bool) -> Vec<u8> {
    let mode = if encrypt {Mode::Encrypt} else {Mode::Decrypt};
    let mut worker = Crypter::new(Cipher::aes_128_ecb(), mode, key, None).unwrap();
    let mut output = vec![0u8; input.len() + 128/8];
    let mut count = worker.update(input, & mut output).unwrap();
    if finalize {
        count += worker.finalize(& mut output[count..]).unwrap();
        output.truncate(count);
    } else {
        output.truncate(input.len());
    }
    output
}

fn detect_aes_ecb(file: &str) -> (u32, u8) {
    let mut statistics = lines_from_file(file).unwrap().enumerate().map(|(i, c)| {
        let blocks = c.unwrap().from_hex().unwrap().chunks(16).map(|chunk| chunk.to_vec()).collect::<Vec<Vec<u8>>>();
        let mut matches = 0;
        for i in 0..blocks.len() {
            for j in i+1..blocks.len() {
                if blocks[i] == blocks[j] {
                    matches += 1;
                }
            }
        }
        (i as u32, matches as u8)
    }).collect::<Vec<(u32, u8)>>();
    statistics.sort_unstable_by(|a, b| b.1.cmp(& a.1));
    statistics[0]
}

fn pkcs7(text: &[u8], blocksize: u8) -> Vec<u8> {
    let size = text.len();
    let padding = blocksize - ((size%(blocksize as usize)) as u8);
    let mut result = text.to_vec();
    for _ in 0..padding {
        result.push(padding);
    }
    result
}

fn unpkcs7(text: &[u8], blocksize: u8) -> Vec<u8> {
    let size = text.len();
    let padding = text[size-1];
    let mut result = text.to_vec();
    if padding <= blocksize {
        if (0..padding).find(|i| text[size - (*i as usize) - 1] != padding) == None {
            for _ in 0..padding {
                result.pop();
            }
        }
    }
    result
}

fn aes_cbc_enc_block(input: &[u8], side: &[u8], key: &[u8]) -> Vec<u8> {
    aes_ecb(&xor(input, side), key, true, false)
}

fn aes_cbc_dec_block(input: &[u8], side: &[u8], key: &[u8]) -> Vec<u8> {
    xor(&aes_ecb(input, key, false, false), side)
}

pub fn aes_cbc(input: &[u8], key: &[u8], iv: &[u8], encrypt: bool) -> Vec<u8> {
    let input = if encrypt { pkcs7(input, 128/8) } else { input.to_vec() };
    // println!("{:?}", input);
    let mut output = vec![0u8; input.len()];
    let len = input.len()/16;
    let block_handler = |i, output: &[u8]| {
        let inner_handler = if encrypt { aes_cbc_enc_block } else { aes_cbc_dec_block };
        let side = if encrypt {
            if i != 0 { &output[16*(i-1)..16*i] } else { iv }
        } else {
            if i != 0 { &input[16*(i-1)..16*i] } else { iv }
        };
        inner_handler(&input[16*i..16*(i+1)], side, key)
    };
    for i in 0..len {
        // println!("{} of {}", i, len);
        let out_block = block_handler(i, &output);
        output[16*i..16*(i+1)].clone_from_slice(&out_block);
    }
    unpkcs7(&output, 16)
}

fn random_key(size: u8) -> Vec<u8> {
    let mut rng = rand::thread_rng();
    let mut result = Vec::<u8>::with_capacity(size as usize);
    for _ in 0..size {
        result.push(rng.gen());
    }
    result
}

fn randomize(data: &mut [u8]) {
    let mut rng = rand::thread_rng();
    for i in data.iter_mut() {
        *i = rng.gen();
    }
}

fn random_bytes(number: usize) -> Vec<u8> {
    let mut result = vec![0u8; number];
    randomize(&mut result);
    result
}

#[derive(PartialEq, Debug)]
pub enum AesMode {
    CBC,
    ECB,
}

pub struct Prediction {
    cipher: Vec<u8>,
    mode: AesMode,
}

pub fn encryption_oracle(input: &[u8]) -> Prediction {
    let mut rng = rand::thread_rng();
    let prepadding_size = rng.gen_range(5, 11);
    let postpadding_size = rng.gen_range(5, 11);
    let mut adjusted_input = vec![0u8; prepadding_size + postpadding_size + input.len()];
    randomize(&mut adjusted_input[0..prepadding_size]);
    adjusted_input[prepadding_size..input.len()+prepadding_size].clone_from_slice(&input);
    let end = adjusted_input.len();
    randomize(&mut adjusted_input[prepadding_size + input.len()..end]);
    let mode = if rng.gen_range(0, 2) == 0 { false } else { true };
    let key = random_bytes(16);
    if mode {
        Prediction{cipher: aes_ecb(&adjusted_input, &key, true, true), mode: AesMode::ECB}
    } else {
        let iv = random_bytes(16);
        Prediction{cipher: aes_cbc(&adjusted_input, &key, &iv, true), mode: AesMode::CBC}
    }
}

pub fn detect_mode(cipher: &[u8]) -> AesMode {
    let blocks = cipher.chunks(16).skip(2).take(2).map(|b| b.to_vec()).collect::<Vec<_>>();
    if blocks[0] == blocks[1] { AesMode::ECB } else { AesMode::CBC }
}

// The idea is that the "signature" is unknown to the user and is used to verify that the
// encryption was done using this method at a later point.
pub struct Encryptor {
    key: Vec<u8>,
    secret: Vec<u8>,
    blocksize: u16,
}

impl Encryptor {
    pub fn new(key: &[u8], secret: &[u8]) -> Encryptor {
        Encryptor{key: key.to_vec(), secret: secret.to_vec(), blocksize: 16}
    }

    pub fn encrypt_and_sign(&self, input: &[u8]) -> Vec<u8> {
        let mut verified_input = input.to_vec();
        verified_input.extend_from_slice(&self.secret);
        aes_ecb(&verified_input, &self.key, true, true)
    }
}

fn calc_blocksize(e: &Encryptor) -> Option<u8> {
    for bs in 1..128 { // bytes
        let msg = vec![0u8; bs*2];
        let signed = e.encrypt_and_sign(&msg);
        let first_blocks = signed.chunks(bs).take(2).map(|b| b.to_vec()).collect::<Vec<_>>();
        if first_blocks[0] == first_blocks[1] {
            return Some(bs as u8);
        }
    }
    return None;
}

fn get_msg_size(e: &Encryptor) -> usize {
    let base_size = e.encrypt_and_sign(&vec![0u8; 0]).len();
    let padding = (1..17).find(|pad| {
        let new_size = e.encrypt_and_sign(&vec![0u8; *pad]).len();
        if new_size > base_size { true } else { false }
    }).unwrap();
    base_size - padding
}

fn get_a_byte(e: &Encryptor, blocksize: usize, prev_bytes: &[u8]) -> Option<u8> {
    let get_block = |data: &[u8], i| data.chunks(blocksize).nth(i).unwrap().to_vec();
    let known_bytes = prev_bytes.len();
    let block_to_test = known_bytes/blocksize as usize;
    let payload_size = blocksize - (known_bytes % blocksize) - 1;
    let payload_block = vec![0u8; payload_size];
    let target_block = get_block(&e.encrypt_and_sign(&payload_block), block_to_test);
    let mut test_block = vec![0u8; blocksize];
    if known_bytes != 0 {
        let bottom_line_dst = if known_bytes >= blocksize - 1 { 0 } else { blocksize - known_bytes - 1 };
        let bottom_line_src = if prev_bytes.len() > blocksize - 1 { prev_bytes.len() - blocksize + 1 } else { 0 };
        test_block[bottom_line_dst..blocksize-1].clone_from_slice(&prev_bytes[bottom_line_src..]);
    }
    for byte in 0u8..255 {
        test_block[blocksize-1] = byte;
        if get_block(&e.encrypt_and_sign(&test_block), 0) == target_block {
            return Some(byte);
        }
    }
    return None;
}

pub fn become_an_admin(auth: &Authorizer) -> Vec<(String, String)> {
    let get_block = |data: &[u8], i| data.chunks(16).nth(i).unwrap().to_vec();
    let pass = |data: &[(String, String)]| {
        let roles = data.iter().filter(|(k, _)| k == "role").collect::<Vec<_>>();
        roles.len() == 1 && roles[0].1 == "admin"
    };
    loop {
        let c0 = auth.provide_cookie("iamanrealaadmin").from_base64().unwrap();
        let c1 = auth.provide_cookie("manreaaladmin").from_base64().unwrap();
        let payload = &[get_block(&c1, 0), get_block(&c1, 1), get_block(&c0, 1), get_block(&c1, 2)].concat();
        let result = auth.parse_cookie(&payload.to_base64(STANDARD));
        if pass(&result) {
            break result;
        }
    }
}

#[cfg(test)]
mod tests {
use aes::*;

#[test]
fn aes_ecb_single() {
    let plaintext = "well ohsubmarine".as_bytes();
    let key = "YELLOW SUBMARINE".as_bytes();
    let cipher = aes_ecb(&plaintext, key, true, true);
    let decipher = aes_ecb(&cipher, key, false, true);
    assert_eq!(plaintext, decipher.as_slice());
}

#[test]
fn aes_ecb_multi() {
    let plaintext = "Well the submarine is owned by Hagbard and the Great Discordian Movement".as_bytes();
    let key = "HAGBARD . CELINE".as_bytes();
    let cipher = aes_ecb(plaintext, key, true, true);
    let decipher = aes_ecb(&cipher, key, false, true);
    assert_eq!(plaintext, decipher.as_slice());
}

#[test]
fn padding_necessary()
{
    let text = "YELLOW SUBMARINE".as_bytes();
    let padded = pkcs7(text, 20);
    assert_eq!(padded.len(), 20);
    assert_eq!(padded[17], 4);
}

#[test]
fn padding_unnecessary()
{
    let text = "YELLOW SUBMARINE".as_bytes();
    let padded = pkcs7(text, 16);
    assert_eq!(padded.len(), 32);
}

#[test]
fn unpadding_correct()
{
    let text = "YELLOW SUBMARINE".as_bytes();
    let padded = pkcs7(text, 16);
    let unpadded = unpkcs7(&padded, 16);
    assert_eq!(text, unpadded.as_slice());
}

#[test]
fn aes_cbc_single_noiv() {
    let plaintext = "well ohsubmarine".as_bytes();
    let key = "YELLOW SUBMARINE".as_bytes();
    let iv = vec![0u8; 16];
    let cipher = aes_cbc(&plaintext, key, &iv, true);
    let decipher = aes_cbc(&cipher, key, &iv, false);
    assert_eq!(plaintext, decipher.as_slice());
}

#[test]
fn aes_cbc_lt_single() {
    let plaintext = "well me".as_bytes();
    let key = "SUBMARINE ELLOWY".as_bytes();
    let iv = vec![5u8; 16];
    let cipher = aes_cbc(&plaintext, key, &iv, true);
    let decipher = aes_cbc(&cipher, key, &iv, false);
    assert_eq!(plaintext, decipher.as_slice());
}

#[test]
fn aes_cbc_single() {
    let plaintext = "well ohsubmarine".as_bytes();
    let key = "SUBMARINE ELLOWY".as_bytes();
    let iv = vec![5u8; 16];
    let cipher = aes_cbc(&plaintext, key, &iv, true);
    let decipher = aes_cbc(&cipher, key, &iv, false);
    assert_eq!(plaintext, decipher.as_slice());
}

#[test]
fn aes_cbc_multi() {
    let plaintext = "well ohsubmarine we are shallow souls are you with me".as_bytes();
    let key = "SUBMARINE ELLOWY".as_bytes();
    let iv = vec![5u8; 16];
    let cipher = aes_cbc(&plaintext, key, &iv, true);
    let decipher = aes_cbc(&cipher, key, &iv, false);
    assert_eq!(plaintext, decipher.as_slice());
}

#[test]
fn random_keys_are_random() {
    assert_ne!(random_key(16), random_key(16));
}

#[test]
fn randomize_randomizes() {
    let mut str = String::from("test.test test.test test.test");
    let mut data = unsafe { str.as_bytes_mut() };
    let mut orig_data = Vec::new();
    orig_data.extend_from_slice(data);
    randomize(&mut data);
    assert_ne!(data, orig_data.as_slice());
}

#[test]
fn oracle_employs() {
    let data = "test test test test test".as_bytes();
    assert_ne!(encryption_oracle(data).cipher, encryption_oracle(data).cipher);
}

#[test]
fn oracle_broken() {
    let data = vec![0u8; 16*10];
    for _iter in 0..100 {
        let prediction = encryption_oracle(&data);
        let estimate = detect_mode(&prediction.cipher);
        assert_eq!(estimate, prediction.mode);
    }
}

mod encryptor_cracker {
use aes::*;
use aes::serialize::base64::FromBase64;
use pieces::authorizer::Authorizer;

#[test]
fn wont_leak_directly() {
    let key = random_bytes(16);
    let secret = b"SECRETS";
    let encryptor = Encryptor::new(&key, secret);
    let cipher = encryptor.encrypt_and_sign(b"data");
    let contains = |haystack: &[u8], needle: &[u8]| {
        'hloop: for i in 0..haystack.len() {
            let mut ti = i;
            for nc in needle.iter() {
                if ti >= haystack.len() {
                    return false;
                }
                if *nc != haystack[ti] {
                    continue 'hloop;
                }
                ti += 1;
            }
            return true;
        }
        return false;
    };
    assert!(contains(b"AAAABA", b"ABA"));
    assert!(!contains(b"AAAABA", b"ABBA"));
    assert!(!contains(&cipher, secret));
}

#[test]
fn can_get_block_size() {
    let key = random_bytes(16);
    let secret = b"SECRETS";
    let encryptor = Encryptor::new(&key, secret);
    assert_eq!(calc_blocksize(&encryptor), Some(16u8));
}

#[test]
fn can_get_secret_length() {
    let key = random_bytes(16);
    let secret = b"YELLOW SUBMARINE";
    let encryptor = Encryptor::new(&key, secret);
    assert_eq!(get_msg_size(&encryptor), secret.len());
}

#[test]
fn can_guess_first_block() {
    let key = random_bytes(16);
    let secret = b"YELLOW SUBMARINE oh YELLOW SUBMARINE";
    let encryptor = Encryptor::new(&key, secret);
    let mut bytes = Vec::new();
    for i in 0..16 {
        let byte = get_a_byte(&encryptor, 16, &bytes).unwrap();
        bytes.push(byte);
        assert_eq!(byte, secret[i]);
    }
}

#[test]
fn can_guess_everything() {
    let key = random_bytes(16);
    let secret = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg\
                  aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq\
                  dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg\
                  YnkK".from_base64().unwrap();
    let encryptor = Encryptor::new(&key, &secret);
    let mut bytes = Vec::new();
    for i in 0..secret.len() {
        let byte = get_a_byte(&encryptor, 16, &bytes).unwrap();
        bytes.push(byte);
        assert_eq!(byte, secret[i]);
    }
}

#[test]
fn can_become_an_admin() {
    let auth = Authorizer::new("YELLOW SUBMARINE".as_bytes());
    become_an_admin(&auth);
}
}
}

