extern crate rustc_serialize as serialize;
extern crate std;
use xor::serialize::hex::{FromHex};
use xor::serialize::base64::{self, ToBase64};
use std::collections::HashMap;
use std::io::{self, BufRead};
use std::path::Path;
use std::fs::File;
use std::str;

pub fn hex2base64(hex: &str) -> String {
    hex.from_hex().unwrap().to_base64(base64::STANDARD)
}

pub fn xor(a: &[u8], b: &[u8]) -> Vec<u8> {
    assert_eq!(a.len(), b.len());
    let mut result : Vec<u8> = Vec::with_capacity(a.len());
    for (i, j) in a.iter().zip(b) {
        result.push(i ^ j);
    }
    result
}

fn unhex(a: &str) -> Vec<u8> {
    let unhex_d = a.trim().from_hex().expect("Unable to unhex.");
    unhex_d
}

fn char_freq(line: &str) -> HashMap<char, f64> {
    let mut occurences: HashMap<char, f64> = HashMap::new();
    for c in line.chars() {
        let c = c.to_lowercase().next().expect("Unable to lowercase");
        let count = occurences.entry(c).or_insert(0f64);
        *count += 1f64;
    }
    for (_, v) in &mut occurences {
        *v /= line.len() as f64;
        *v /= 100f64;
    }
    occurences
}

fn freq_metric(base: &HashMap<char, f64>, sample: &HashMap<char, f64>) -> f64 {
    let mut metric = 0f64;
    for (base_c, base_f) in base.iter() {
        let sample_f = match sample.get(base_c) {
            Some(v) => *v,
            None => 0_f64,
        };
        metric += (base_f - sample_f).abs();
    }
    metric
}

fn make_base_freqs() -> HashMap<char, f64> {
    let mut result: HashMap<char, f64> = HashMap::new();
    let letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                   'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                   'y', 'z'];
    let freqs = [8.167, 1.492, 2.782, 4.253, 12.70, 2.228, 2.015, 6.094, 6.966,
                         0.153, 0.772, 4.025, 2.406, 6.749, 7.507, 1.929, 0.095, 5.987,
                         6.327, 9.056, 2.758, 0.978, 2.360, 0.150, 1.974, 0.074];
    for (l, f) in letters.iter().zip(freqs.iter()) {
        result.insert(*l, *f);
    }
    result
}

fn single_byte_xor_solve(cipher: &[u8]) -> (String, f64, u8) {
    let mut best_metric = std::f64::MAX;
    let mut result = String::new();
    let base_freqs = make_base_freqs();
    let mut k = 0u8;
    for i in 0..255 {
        let key: Vec<u8> = vec![i; cipher.len()];
        let decipher = xor(cipher, key.as_slice());
        let decipher_readable = match String::from_utf8(decipher) {
            Ok(v) => v,
            Err(_) => continue,
        };
        let m = freq_metric(&base_freqs, &char_freq(&decipher_readable));
        if m < best_metric {
            best_metric = m;
            result = decipher_readable;
            k = i as u8;
        }
    }
    if k == 0u8 {
        // For some reason spaces aren't guessed correctly.
        k = ' ' as u8;
    }
    (result, best_metric, k)
}

pub fn lines_from_file<F>(filename: F) -> Result<io::Lines<io::BufReader<File>>, io::Error> where
    F: AsRef<Path>,
{
    let file = try!(File::open(filename));
    Ok(io::BufReader::new(file).lines())
}

